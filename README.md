# Mathe-Maister
Ansammlung einiger wichtiger themenübergreifender mathematischer Definitionen und Sätze, später dann verpackt in Anki in schönem Latex, um die Sprache der Mathematik fließender zu verstehen.

Hilfe zu vielen allgemeinen und speziellen Fragen gibt es [im Hilfedokument](/Anleitungen-Links-etc/Hilfe-Allgemein.md).


# Anki-Decks
Inhaltliche Korrektheit ohne Gewähr, vollständige Sammlungen teilweise durch Promovierende o.ä. korrigiert:  

Laufend:
  + [**Optimierung III**](https://cptmaister.gitlab.io/Mathe-Maister/Optimierung-III.apkg)

Pausiert:
  + [**Analysis I**](https://cptmaister.gitlab.io/Mathe-Maister/Analysis-I.apkg)
  + [**Topologie**](https://cptmaister.gitlab.io/Mathe-Maister/Topologie.apkg)
  + [**Mathe Basics** (Sammlung oft vergessener Tricks und Basics)](https://cptmaister.gitlab.io/Mathe-Maister/Mathe-Basics.apkg)

Fertig oder abgebrochen bzw. nicht ganz fertig:
  + [**Ana LA Grundlagen** (fertig)](https://cptmaister.gitlab.io/Mathe-Maister/Ana-LA-Grundlagen.apkg)
  + [**Lineare Algebra I** (fertig, WiSe 18/19 Uni Augsburg, bis ausschließlich Eigenwerte)](https://cptmaister.gitlab.io/Mathe-Maister/Lineare-Algebra-I.apkg)
  + [**Numerik I** (fertig, WiSe 18/19 Uni Augsburg, bis ausschließlich Interpolation durch Splines)](https://cptmaister.gitlab.io/Mathe-Maister/Numerik-I.apkg)
  + [**Analysis III**](https://cptmaister.gitlab.io/Mathe-Maister/Analysis-III.apkg)
  + [**Geometrie I**](https://cptmaister.gitlab.io/Mathe-Maister/Geometrie-I.apkg)
  + [**Funktionentheorie**](https://cptmaister.gitlab.io/Mathe-Maister/Funktionentheorie.apkg)
  + [**Seminar Geometrie von Flächen**](https://cptmaister.gitlab.io/Mathe-Maister/Seminar-Geometrie-von-Flaechen.apkg)
  + [**Seminar zu Differentialformen**](https://cptmaister.gitlab.io/Mathe-Maister/Seminar-Differentialformen.apkg)

# PDFs  
Hier sind die Links zu für Menschen schön lesbaren PDFs aller Stoffsammlungen:   

Laufend:
  + [**Optimierung III**](https://cptmaister.gitlab.io/Mathe-Maister/Optimierung-III.pdf)

 Pausiert:
  + [**Analysis I**](https://cptmaister.gitlab.io/Mathe-Maister/Analysis-I.pdf)
  + [**Topologie**](https://cptmaister.gitlab.io/Mathe-Maister/Topologie.pdf)
  + [**Mathe Basics** (Sammlung oft vergessener Tricks und Basics)](https://cptmaister.gitlab.io/Mathe-Maister/Mathe-Basics.pdf)

 Fertig oder abgebrochen bzw. nicht ganz fertig:
  + [**Ana LA Grundlagen** (fertig)](https://cptmaister.gitlab.io/Mathe-Maister/Ana-LA-Grundlagen.pdf)
  + [**Lineare Algebra I** (fertig, WiSe 18/19 Uni Augsburg, bis ausschließlich Eigenwerte)](https://cptmaister.gitlab.io/Mathe-Maister/Lineare-Algebra-I.pdf)
  + [**Numerik I** (fertig, WiSe 18/19 Uni Augsburg, bis ausschließlich Interpolation durch Splines)](https://cptmaister.gitlab.io/Mathe-Maister/Numerik-I.pdf)
  + [**Analysis III**](https://cptmaister.gitlab.io/Mathe-Maister/Analysis-III.pdf)
  + [**Geometrie I**](https://cptmaister.gitlab.io/Mathe-Maister/Geometrie-I.pdf)
  + [**Funktionentheorie**](https://cptmaister.gitlab.io/Mathe-Maister/Funktionentheorie.pdf)
  + [**Seminar Geometrie von Flächen**](https://cptmaister.gitlab.io/Mathe-Maister/Seminar-Geometrie-von-Flaechen.pdf)
  + [**Seminar zu Differentialformen**](https://cptmaister.gitlab.io/Mathe-Maister/Seminar-Differentialformen.pdf)


# Sonstiges

<!-- ## > Wie erfahre ich von Neuigkeiten?
Dafür gibt es [*diesen E-Mail-Verteiler*](http://supersecret.me/ankiprojekt/), auf dem man sich jederzeit ein- und austragen kann. -->


## Website muss repariert werden
Die 2019 aufgesetzte Website habe ich irgendwie kaputt gemacht. Vorerst gibt's also alle Infos wieder hier auf gitlab, bi sich sie repariert habe.


## Special Thanks
+ [Daniel A](https://gitlab.com/esclear) :ant: ([CSV Checker Skript](MMskripte/csv-checker.py))
+ Felix W :owl: (Early Testing)
+ [Ingo Blechschmidt](https://gitlab.com/iblech) :turtle: (Korrektur Deluxe, Motivation, ehem. Deckspeicher)
+ Julia G :bird: (Lineare Algebra I)
+ [Kilian R](https://gitlab.com/kiandru) :unicorn: (Readme-Layout-Spezialist, Blockchain Entrepreneur)
+ Leonie N :shark: (Motivation, Beratung) 
+ [Michael Struwe](https://gitlab.com/shak-mar) :frog: ([PDF Skripte](MMskripte), [CI](.gitlab-ci.yml), git Setup, [Licensing](LICENSE))
+ Moritz H :wolf: (Numerik I, [Mailing](http://supersecret.me/ankiprojekt/))
+ [Rainer E](https://gitlab.com/fanaticae) :leopard: (SQL Hacker, Python-Connaisseur, [APKG Skript](MMskripte), [CI](.gitlab-ci.yml), Ana3)
+ Roland M :dragon_face: (Korrektur Numerik I)
+ [Xaver H](https://gitlab.com/xaverdh) :dolphin: (1 Meister, [Docker](docker))


### Kontakt
**alex.mai@posteo.net**


## Copyright Notice
    Mathe-Maister
    Copyright (C) 2019  Alexander Mai

    This information is free: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Additionally you have to include the URL to this gitlab project 
    <https://gitlab.com/CptMaister/Mathe-Maister/> or the website 
    <https://mathe-maister.de> conspicuously. If you are distributing 
    the apkg-files of this project, you are further required to either
    leave the first note untouched and unhidden from the user, as it
    contains a link to this project. Or, if you want to change the
    content of the first note for any reasons such as for example for
    translation, you have to include the link to this project in a field
    of the note that is clearly readable when using it on Anki (the
    official desktop client, at least version 2.0) and when using it on
    AnkiDroid or Anki Mobile either in an immediately visible field or
    in an easy to see and click hint field.

    This work is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

