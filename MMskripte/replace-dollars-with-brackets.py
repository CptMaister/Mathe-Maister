#!/usr/bin/env python3

"""This was written for one time use on all old CSVs, as Anki got updated and no longer supports LaTeX math mode in $ $ and $$ $$ but instead uses \\( \\) and \\[ \\]
"""

import sys, re

csv_path = sys.argv[1]

new_lines = []

line_counter = 0

with open(csv_path) as csv_file:
    for line in csv_file:
        
        line_counter = line_counter + 1

		# Ignore lines beginning with #
        if line.startswith('#'):
            new_lines.append(line.strip("\n"))
            continue

		# Split line at tabs
        line_all_fields = line.split('\t')
		#name, desc, notes, lecture, card_type, topic, formula, interpretation, fun_facts, trivia, examples, hint, extras, literature, translation, front_hint, back_hint, illustration, tags, note_id, single_sided, priority = line_all_fields
        
        new_line = []
        
        for field in line_all_fields:
            
            while field.find("$$") != -1:
                field = field.replace("$$", "\[", 1)
                if field.find("$$") == -1:
                    message = f"""    {field}
                    Missing second $$, line {line_counter}"""
                    print(message)
                field = field.replace("$$", "\]", 1)
			
            while field.find("$") != -1:
                field = field.replace("$", "\(", 1)
                if field.find("$") == -1:
                    message = f"""    {field}
                    Missing second $, line {line_counter}"""
                    print(message)
                field = field.replace("$", "\)", 1)
            
            new_line.append(field)
        
        new_lines.append('\t'.join(new_line).strip("\n"))

#if len(new_lines) > 4:
#    print(new_lines[1])
#    print(new_lines[4])

with open(csv_path, 'r+') as f:
    f.seek(0)
    f.truncate()
    f.write("\n".join(new_lines))
    f.close()




