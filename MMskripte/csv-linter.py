#!/usr/bin/env python3

import sys, re

csv_path = sys.argv[1]

#Create Header
header_how_to = "How to:\nUse this document to find errors that have been known to cause trouble but haven't been fixed (so far).\nHere every chapter/topic will be split by a headline proclaiming the topic's name, followed by every line in the CSV that is neither empty nor commented out using a '#'. Then every line's number and note id will either be followed by nothing, which means the linter can't find any problems. Or if there are any problems or suspicious constructs, there will be a short error message accompanied by a capital-letter-code (like ID-FORMAT or BOLD) that you can use to look up a solution in the Look-Up Section at the very bottom. Errors known to be hard to find are marked with a '!!!', so search for !!! first if you don't know what to do. If you have triggered (by accident or not) a feature of the APKG-script, the line will have a message saying so but with a lower-case-code."
header_lookup = "Look-up:\nLocated at the bottom. There you will find explanations on all error codes and what to do."
header_unknown_problem = "Problem not resolved:\nThis linter won't find tex errors. So if you typed \mathbb[N} or \mapstop somewhere, this won't be shown here. If there are still any unexpected errors with your CSV although the linter doesn't recognize it and you can't find it anywhere, please contact alex.mai@posteo.net with your problem. We'll try to find your problem and make the linter recognize it."
header_tex_errors = "Checking tex errors:\nTo check for latex errors, check the log in the Pipeline for the create_pdf job. You can access it like this: Head to the main page of the repository, in the left-hand sidebar hover your mouse ofer 'CI / CD', then click on 'Jobs'. There click on the newest entry that says 'create_pdf'."
header_notice = "Important notice:\nEmpty Lines should always be avoided, as they can lead to problems in the PDF creation. Make sure to check if the empty line count is at 0. The amount of empty and comment lines is given further below after the last topic.\nDon't save your CSV files in an ODF format or anything like this. Always use plain text CSV files."

print("This is the linter output for the file {0}. Check gitlab.com/CptMaister/Mathe-Maister for more info.\n\n{1}\n\n{2}\n\n{3}\n\n{4}\n\n{5}\n\n".format(csv_path, header_how_to, header_lookup, header_unknown_problem, header_tex_errors, header_notice))
# ---end Header

comment_count = 0
empty_line_count = 0
topic_current = ''
line_count = 0
critical_error_count = 0
error_count = 0
single_sided_count = 0
tex_comment_count = 0
placeholder_count = 0
unfinished_line_count = 0

ids = []

print('---------------------------------------------------------')

with open(csv_path) as csv_file:
	for line in csv_file:

		line_count += 1

		#Highlight empty lines
		if line.replace('\t', '').replace('\n', '') == '':
			print('EMPTY: avoid empty lines, use # to comment.')
			empty_line_count += 1
			continue

		# Ignore lines beginning with a #
		if line.startswith('#'):
			comment_count += 1
			continue

		# Split line at tabs
		line_all_fields = line.split('\t')
		name, desc, notes, lecture, card_type, topic, formula, interpretation, fun_facts, trivia, examples, hint, extras, literature, translation, front_hint, back_hint, illustration, tags, note_id, single_sided, priority = line_all_fields

		# Highlight beginning of a new topic
		if topic_current != topic:
			topic_current = topic
			print('\n\t\tStart topic {}.'.format(topic))

		#print line number and ID of this line
		print('\tline {0}, id {1}'.format(line_count, note_id))

		# check for double ID occurences
		if note_id in ids:
			print("!!! ID-DOUBLE: Note id {} is a duplicate!".format(note_id))
			critical_error_count += 1
		# add this id to the list of ids to check
		if note_id:
			ids += [note_id]

		# note_id is not a valid number
		if not re.match(r"^(\d+(\.\d{,2})?)?$", note_id):
			critical_error_count += 1
			print('!!! ID-FORMAT: id {} (maybe) not a valid number'.format(note_id))

	#check for special APKG script behavior

		# Highlight lines that are ignored by the APKG script
		if name == '' or desc == '' or lecture == '' or note_id == '':
			unfinished_line_count += 1
			print('APKG-IGNORED: Name, description, lecture or ID missing')

		# Highlight one-sided notes
		if ('1' in single_sided) and ('2' in single_sided):
			error_count += 1
			print("SINGLE-SIDED-DOUBLE: the single-sided field should only contain '1' or '2', you have {}".format(single_sided))
		else :
			if '1' in single_sided:
				single_sided_count += 1
				print("single-sided-front: this note will only have a Name->Description card - input: {}".format(single_sided))
			if '2' in single_sided:
				single_sided_count += 1
				print("single-sided-back: this note will only have a Description->Name card - input: {}".format(single_sided))

	# check for possible errors

		# check for not closed <b>bold expressions</b> (or <i>)
		if line.count('<b>') != line.count('</b>'):
			critical_error_count += 1
			print('!!! BOLD: <b> not properly closed by a </b> somewhere in this line')
		
		if line.count('<i>') != line.count('</i>'):
			critical_error_count += 1
			print('!!! ITALIC: <i> not properly closed by a </i> somewhere in this line')

		# check field illustration
		if illustration and not re.match(r"([a-zA-Z0-9_()-]+\.(png|svg|jpg) +)*[a-zA-Z0-9_()-]+\.(png|svg|jpg)", illustration):
			print("!!! ILLUSTRATION: only svg, png and jpg work, separate with space")
			critical_error_count += 1

        		# check if any field has weird constructs of $, as in the linter checks if there are any occurences of $ not closed or opened properly
		for field in line_all_fields:
			if not re.match(r"(^\$\$[^$]+\$\$$)|(^\$[^$]+\$$)|^(((\$\$[^$]+\$\$)|(\$[^$]+\$)|[^$])*)$", field):
			# \$\$[^$]+\$[^$]|[^$]\$[^$]+\$\$
				error_count += 1
				print("MATH-MODE: somewhere here $$ or $ wasn't properly closed:\n{}".format(field))
		# (accidental?) tex comment
		if '%' in line:
			tex_comment_count += 1
			print('tex-comment: used % somewhere in this line, used to comment in math-mode')

		# forgot to replace NNN, QQQ, RRR, CCC or any other 3 or more capital letter placeholder
		if re.match(r".*([A-Z])\1{2,}.*", line):
			placeholder_count += 1
			print('placeholder: used 3 or more capital letters consecutively')

		# if line == 'exact match':
		# 	do stuff

		# if 'substring' in line:
		# 	do stuff

		# if line.startswith('beginning'):
		#	do stuff

		# if hint == '':
		#	do stuff

		# if name.replace('$', '') == 'foo':
		#	do stuff

print('\n---------------------------------------------------------')


lookup_id_double = "ID-DOUBLE: Never use the same ID twice!!"
lookup_id_format = "ID-FORMAT: The id of a note in the CSV can only be an integer (which is small enough: we have only experimented with 5-digit integers so far, making IDs like 17028 possible, but 201036 might also work). But you can optionally have up to two extra digits, like 15004.12 or 1002.7 etc."
lookup_illustration = "ILLUSTRATION: Only use svg, png or jpg files. You can write, for example, konvergenz.svg and you can include multiple images separated by a single space. Don't use accented letters, Umlaute or anything other than basic ascii letters, numbers, - and _. Using multiple images has not been extensively tested so far. Technically Anki will handle images in any field, but the PDF script can only handle images in the illustration field."
lookup_bold = "BOLD: Whenever you write <b> in a field to start bold text, make sure to finish it with </b> within the same field. Anki natively supports this, as it supports HTML, so it won't be a huge issue for your APKG files. But the PDF files will behave unexpectedly, as to create the PDF every bit of HTML will be converted into the corresponding tex-command kinda sloppily. <b> is converted into \\textbf{ and </b> is converted into }"
lookup_italic = "ITALIC: see BOLD for an explanation. <i> is converted into \\textit{ and </i> is converted into }"
lookup_apkg_ignored = "APKG-IGNORED: The APKG script ignores lines that miss one of the following columns: Name, Description, Lecture name, ID. This ensures that there are no empty notes or notes without a clearly assigned id. The field Lecture name is obligatory so editors of CSV files can easily enable or disable a notes appearance in the APKG file without deleting crucial information, like the id or the description. Use this feature for unfinished lines. Making the line a comment in the CSV would also remove it from the PDF, while this solution doesn't."
lookup_empty = "EMPTY: Empty lines can cause gaps or placeholder text in the PDF. Avoid empty lines, instead use # so they will be ignored."
lookup_math_mode = "MATH-MODE: Whenever you write $ (or $$) in a field to enter math-mode, it should be closed by $ (or $$) within the same field. Not doing so will affect only the note of this line in Anki, but might affect the whole PDF."
lookup_single_sided_double = "SINGLE-SIDED-DOUBLE: the single-sided field is supposed to be a string containing either a '1' or a '2', but not both. If it contains a '1' (but not a '2') (so it could be for example 'card 1', '1', 'K1'), only the Name->Description card will be created. If it contains a '2' (but not a '1'), only the Description->Name card will be created. If it contains '1' and '2', both directions will be created, as if it was empty. This does not affect the PDF and is not saved anywhere in the APKG-file other than by the lack of the other direction of the card."
lookup_single_sided_front = "single-sided-front: see SINGLE-SIDED-DOUBLE for an explanation."
lookup_single_sided_back = "single-sided-back: see SINGLE-SIDED-DOUBLE for an explanation."
lookup_tex_comment = "tex-comment: % is used to comment things in tex. If used accidentally within math-mode of some tex stuff, everything afterwards (up to the end of math-mode) will disappear in the PDF and be invisible in Anki. Maybe accidentally used instead of & in &lt; or &gt; or instead of $?"
lookup_placeholder = "placeholder: three capital letters used 3 times or more in a row is often used as a placeholder, like RRR for \\mathbb{R}, so don't forget to replace them when you're done"

lookup_all = [lookup_id_double, lookup_id_format, lookup_illustration, lookup_bold, lookup_italic, lookup_math_mode, lookup_apkg_ignored, lookup_empty, lookup_single_sided_double, lookup_single_sided_front, lookup_single_sided_back, lookup_tex_comment, lookup_placeholder]

newline_with_tab = "\n\t"
print('\n\nLook-Up Section:\n\t{}'.format(newline_with_tab.join(lookup_all)))

# for proper sentences
def plural_s(count):
	if count == 1:
		return ''
	else:
		return 's'

def message(count, text):
	if count >= 1:
		return ' ' + text
	else:
		return ''
def message_extra(count, extra, text1, text2):
	if count >= extra:
		return ' ' + text2
	elif count >= 1:
		return ' ' + text1
	else:
		return ''


print('\n\n\tError report:')
print('{} critical error{} found.{}'.format(critical_error_count, plural_s(critical_error_count), message_extra(critical_error_count, 25, 'High risk of PDF or APKG script failing.', 'Don\'t blow it. Don\'t do drugs. Stop it. get some help.')))
print('{} error{} found.{}'.format(error_count, plural_s(error_count), message(error_count, 'Might cause unexpected behavior.')))
print('{} placeholder{} found.{}'.format(placeholder_count, plural_s(placeholder_count), message(placeholder_count - 2, 'Pathetic.')))
print('{} single-sided note{}.'.format(single_sided_count, plural_s(single_sided_count)))
print('{} tex-comment{} found.{}'.format(tex_comment_count, plural_s(tex_comment_count), message(tex_comment_count, 'No comment.')))
print('{} unfinished line{}.{}'.format(unfinished_line_count, plural_s(unfinished_line_count), message(unfinished_line_count, 'Someone\'s waiting ...')))
print('{} empty line{}.{}'.format(empty_line_count, plural_s(empty_line_count), message(empty_line_count, 'Creates useless placeholder entries in the PDF.')))
print('{} line{} commented out.'.format(comment_count, plural_s(comment_count)))
