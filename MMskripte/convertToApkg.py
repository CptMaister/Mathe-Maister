#!/usr/bin/env python3

# convertToApkg.py converts the csv files as used in Mathe-Maister (see gitlab.com/CptMaister/Mathe-Maister) to Anki APKG files 
# Copyright (C) 2019  Rainer (rainer@kurosekai.net)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
    


import sqlite3
import time
import json
import re
from hashlib import sha1
from html.entities import name2codepoint
import zipfile
from tempfile import mkdtemp
import sys
import os
import shutil
import json
import io

#store generalconfig.json in same directory as this file
#some infos on the last few entries in this json-file:
#"csv_minEntries": 20, #used to ignore lines where there is nothing in nor after this column
#"csv_entryId": 20, #column in which the ID is, as counted by a human, starting with 1. The id of every line should be a unique integer (when multiplied by 100) within this csv as it is used to create IDs that Anki uses to identify which notes to update. A line corresponding to a certain Anki note should keep its ID forever or there will be weird update conflicts.
#"csv_entryTags": 19, #column in which the Tags are, as counted by a human, starting with 1.
#"csv_entryRelevant": 18, #amount of columns that are relevant, i.e. filled into the fields, not counting tags. Make sure to have at least this many fields in your note type. This script takes the first csv_entryRelevant many columns of every line and puts them into the fields in the same order.
#"csv_entryOneSided": 21 #column in which the information for notes with only one direction of flash cards is
generalconfig = {}

configuration = {}
# {
#     "model_id": 1398130163168,
#     "model_name": "Model Name",
#     "deck_id": 1550006797833,
#     "deck_description": "Einführung in die Numerik bei Prof Daniel Peterseim, WiSe 18/19, Uni Augsburg.\nHauptsächlich ohne Abschätzungen, Konvergenzkriterien und Rechenaufwand. Inhaltlich bis ausschließlich Interpolation durch Splines.\n\nalex.m.s@gmx.de für Fragen und Änderungsvorschläge.\noder gitlab.com/CptMaister\n\nBesonderen Dank an Michael, Moritz und Roland!\n(V2.2)",
#     "deck_name": " Mathe-Maister::Numerik I - TEST",
#     "start_note_id": 123456,
#     "start_card_id": 654321,
#     "guid_random_string": "asdf" #TODO: Make better
# }

def timestamp():
    return int(round(time.time()))


TABLE_COL = """
CREATE TABLE col (
    id              integer primary key,
    crt             integer not null,
    mod             integer not null,
    scm             integer not null,
    ver             integer not null,
    dty             integer not null,
    usn             integer not null,
    ls              integer not null,
    conf            text not null,
    models          text not null,
    decks           text not null,
    dconf           text not null,
    tags            text not null
); """

TABLE_NOTES = """
CREATE TABLE notes (
    id              integer primary key,   /* 0 */
    guid            text not null,         /* 1 */
    mid             integer not null,      /* 2 */
    mod             integer not null,      /* 3 */
    usn             integer not null,      /* 4 */
    tags            text not null,         /* 5 */
    flds            text not null,         /* 6 */
    sfld            integer not null,      /* 7 */
    csum            integer not null,      /* 8 */
    flags           integer not null,      /* 9 */
    data            text not null          /* 10 */
);"""


TABLE_CARDS = """
CREATE TABLE cards (
         id              integer primary key,   /* 0 */
         nid             integer not null,      /* 1 */
         did             integer not null,      /* 2 */
         ord             integer not null,      /* 3 */
         mod             integer not null,      /* 4 */
         usn             integer not null,      /* 5 */
         type            integer not null,      /* 6 */
         queue           integer not null,      /* 7 */
         due             integer not null,      /* 8 */
         ivl             integer not null,      /* 9 */
         factor          integer not null,      /* 10 */
         reps            integer not null,      /* 11 */
         lapses          integer not null,      /* 12 */
         left            integer not null,      /* 13 */
         odue            integer not null,      /* 14 */
         odid            integer not null,      /* 15 */
         flags           integer not null,      /* 16 */
         data            text not null          /* 17 */
     );
"""

TABLE_REVLOG = """ 
CREATE TABLE revlog (
    id              integer primary key,
    cid             integer not null,
    usn             integer not null,
    ease            integer not null,
    ivl             integer not null,
    lastIvl         integer not null,
    factor          integer not null,
    time            integer not null,
    type            integer not null
);
"""

TABLE_GRAVES = """ 
CREATE TABLE graves (
    usn             integer not null,
    oid             integer not null,
    type            integer not null
);
"""

INDEX = [ 
    "CREATE INDEX ix_notes_usn on notes (usn);", 
    "CREATE INDEX ix_cards_usn on cards (usn);",
    "CREATE INDEX ix_revlog_usn on revlog (usn);",
    "CREATE INDEX ix_cards_nid on cards (nid);",
    "CREATE INDEX ix_cards_sched on cards (did, queue, due);",
    "CREATE INDEX ix_revlog_cid on revlog (cid);",
    "CREATE INDEX ix_notes_csum on notes (csum);"
]


def create_tables(database_path):
    with sqlite3.connect(database_path) as con:
        con.execute(TABLE_COL)
        con.execute(TABLE_NOTES)
        con.execute(TABLE_CARDS)
        con.execute(TABLE_REVLOG)
        con.execute(TABLE_GRAVES)
        for index in INDEX:
            con.execute(index)


#Change amount of anki note fields here
def create_col(database_path):
    SQL_STRING = """INSERT INTO col VALUES(:id,:crt,:mod,:scm,:ver,:dty,:usn,:ls,:conf,:models,:decks,:dconf,:tags)"""
    conf = {
        "nextPos":1,
        "estTimes":True,
        "activeDecks":[ 1 ],
        "sortType":"noteFld",
        "timeLim":0,
        "sortBackwards":False,
        "addToCur":True,
        "curDeck":1,
        "newBury":True,
        "newSpread":0,
        "dueCounts":True,
        "curModel":configuration["model_id"],
        "collapseTime":1200        
    }
    models = {
        configuration["model_id"]: {
            "vers":[],
            "name":configuration["model_name"],
            "tags":[],
            "did": configuration["deck_id"],
            "usn":-1,
            "req": [
                [
                    0,
                    "any",
                    [
                        0,
                        3,
                        4,
                        5,
                        15
                    ]
                ],
                [
                    1,
                    "any",
                    [
                        1,
                        3,
                        4,
                        5,
                        16
                    ]
                ]
            ],
            "flds": [
                {
                    "name": name,
                    "rtl": False,
                    "sticky": False,
                    "media": [],
                    "ord": index,
                    "font": "Arial",
                    "size": 20
                }
                for index, name in zip(range(len(generalconfig["fields"])), generalconfig["fields"])
            ],
            "tmpls": [
                {
                    "name": cards["name"],
                    "qfmt": cards["front"],
                    "did": None,
                    "bafmt": "",
                    "afmt": cards["back"],
                    "ord": index,
                    "bqfmt": ""
                }
                for index, cards in zip(range(len(generalconfig["cards"])), generalconfig["cards"])

            ],
            "css": ".card {\n font-family: arial;\n font-size: 20px;\n text-align: center;\n color: black;\n background-color: white;\n}\n",
            "latexPre": "\\documentclass[12pt]{article}\n\\special{papersize=3in,5in}\n\\usepackage[utf8]{inputenc}\n\\usepackage{amssymb,amsmath}\n\\pagestyle{empty}\n\\setlength{\\parindent}{0in}\n\\begin{document}\n",
            "sortf":0,
            "latexPre":"\\documentclass[12pt]{article}\n\\special{papersize=3in,5in}\n\\usepackage{amssymb,amsmath}\n\\pagestyle{empty}\n\\setlength{\\parindent}{0in}\n\\begin{document}\n",
            "latexPost":"\\end{document}",
            "type":0,
            "id":configuration["model_id"],
            "mod":timestamp(),
            "latexsvg": False
        }
    }
    decks = {
        "1": {
            "desc": "",
            "name": "Default",
            "extendRev": 77, #Change max repetitions per day (doesn't work :-( )
            "usn": 0,
            "collapsed": False,
            "newToday": [
                0,
                0
            ],
            "timeToday": [
                0,
                0
            ],
            "dyn": 0,
            "extendNew": 20, #Change max new cards per day (doesn't work for me :-( )
            "conf": 1,
            "revToday": [
                0,
                0
            ],
            "lrnToday": [
                0,
                0
            ],
            "id": 1,
            "mod": 1550006860
        },
        configuration["deck_id"]: {
            "desc": configuration["deck_description"],
            "name": configuration["deck_name"],
            "extendRev": 50,
            "usn": -1,
            "collapsed": False,
            "newToday": [
                42,
                0
            ],
            "timeToday": [
                42,
                0
            ],
            "dyn": 0,
            "extendNew": 10,
            "conf": 1,
            "revToday": [
                42,
                0
            ],
            "lrnToday": [
                42,
                0
            ],
            "id": configuration["deck_id"],
            "mod": timestamp()
        }
    }
    dconf = {
        "1": {
            "name": "Default",
            "replayq": True,
            "lapse": {
                "leechFails": 80, #Change leech limit (doesn't work for me :-( )
                "minInt": 1,
                "delays": [
                    10
                ],
                "leechAction": 0,
                "mult": 0
            },
            "rev": {
                "perDay": 200,
                "fuzz": 0.05,
                "ivlFct": 1,
                "maxIvl": 36500,
                "ease4": 1.3,
                "bury": False,
                "minSpace": 1
            },
            "timer": 0,
            "maxTaken": 60,
            "usn": 0,
            "new": {
                "perDay": 20,
                "delays": [
                    1,
                    10
                ],
                "separate": True,
                "ints": [
                    1,
                    4,
                    7
                ],
                "initialFactor": 2500,
                "bury": False,
                "order": 1
            },
            "mod": 0,
            "id": 1,
            "autoplay": True
        }
    }

    values = {
        "id":1,    
        "crt":timestamp(),   
        "mod":timestamp(), 
        "scm":timestamp(),   
        "ver":11,   
        "dty":0,   
        "usn":0,   
        "ls":0,    
        "conf": json.dumps(conf),  
        "models":json.dumps(models),
        "decks":json.dumps(decks),
        "dconf":json.dumps(dconf),
        "tags": "{}"
    }
    
    with sqlite3.connect(database_path) as con:
        con.execute(SQL_STRING, values)
        

def create_notes(database_path, csv_path):
    NOTES_SQL = """INSERT INTO notes VALUES(:n_id,:n_guid,:n_mid,:n_mod,:n_usn,:n_tags,:n_flds,:n_sfld,:n_csum,:n_flags,:n_data)"""
    CARDS_SQL = """INSERT INTO cards VALUES(:c_id,:c_nid,:c_did,:c_ord,:c_mod,:c_usn,:c_type,:c_queue,:c_due,:c_ivl,:c_factor,:c_reps,:c_lapses,:c_left,:c_odue,:c_odid,:c_flags,:c_data)"""

    def is_valid_line(items):
        # Test if column number csv_minEntries or after is not empty
        # Test if the entry csv_entryId (multiplied by 100) is a number
        # Test if first and second and fourth entry are not empty
        return len(items) >= generalconfig["csv_minEntries"] and (items[generalconfig["csv_entryId"] - 1] * 100).isdigit() and len(items[0]) > 0 and len(items[1]) > 0 and len(items[3]) > 0
    
    # split by lines (one entry per line), ignore lines starting with #, strip off "\n" at the end of each line and separate at TAB
    with open(csv_path) as file:
        lines = filter(is_valid_line, map(lambda x: x.rstrip("\n").split("\t"), filter(lambda x: not x.startswith("#"), file.readlines()))) 

    with sqlite3.connect(database_path) as con: 
        for index, entry in enumerate(lines):
            modified = timestamp() + 31104000

            # this adds the HTML stuff to the illustration field
            illustration_index = generalconfig["csv_entryIllustration"] - 1
            if entry[illustration_index]:
                entry[illustration_index] = " ".join(map(lambda image: '<img src="%s" />' % image, entry[illustration_index].split(" ")))
            
            note = {
                #"First create the note:"
                # multiply IDs by 100 to allow 2 extra digits for later inserted entries
                "n_id": (int(entry[generalconfig["csv_entryId"] - 1]) * 100) + configuration["start_note_id"],
                "n_guid": str(int(entry[generalconfig["csv_entryId"] - 1]) * 100) + configuration["guid_random_string"],
                "n_mid": configuration["model_id"],
                "n_mod": modified,
                "n_usn": -1,
                "n_tags": entry[generalconfig["csv_entryTags"] - 1],
                "n_flds": "\x1f".join(entry[:generalconfig["csv_entryRelevant"]]),
                "n_sfld": entry[0],
                "n_csum": fieldChecksum(entry[0]),
                "n_flags": 0,
                "n_data": ""
            }
            con.execute(NOTES_SQL, note)

            # check entry after ID for a string containing "1" (but not "2") or "2" (but not "1"), meaning "create only card 1" or card 2 respectively
            entry_one_sided = entry[generalconfig["csv_entryOneSided"] - 1]
            one_sided_contains_one = "1" in entry_one_sided
            one_sided_contains_two = "2" in entry_one_sided

            #print(len(entry))

            if (not one_sided_contains_two) or (one_sided_contains_one and one_sided_contains_two):
                card = {
                    #Then the associated card1: 
                    "c_id": (int(entry[generalconfig["csv_entryId"] - 1]) * 100) + configuration["start_card_id"],
                    "c_nid": (int(entry[generalconfig["csv_entryId"] - 1]) * 100) + configuration["start_note_id"],   
                    "c_did": configuration["deck_id"],
                    "c_ord": 0,
                    "c_mod": modified,
                    "c_usn": -1, 
                    "c_type": 0,
                    "c_queue": 0,
                    "c_due": index,
                    "c_ivl": 0,
                    "c_factor": 0, 
                    "c_reps": 0, 
                    "c_lapses": 0,
                    "c_left": 0,
                    "c_odue": 0,
                    "c_odid": 0, 
                    "c_flags": 0,
                    "c_data": ""
                }
                con.execute(CARDS_SQL, card);
            if (not one_sided_contains_one) or (one_sided_contains_one and one_sided_contains_two):
                card = {
                    #Then the associated card2: 
                    "c_id": ((int(entry[generalconfig["csv_entryId"] - 1]) * 100) + configuration["start_card_id"]) * 2,
                    "c_nid": (int(entry[generalconfig["csv_entryId"] - 1]) * 100) + configuration["start_note_id"],   
                    "c_did": configuration["deck_id"],
                    "c_ord": 1,
                    "c_mod": modified,
                    "c_usn": -1, 
                    "c_type": 0,
                    "c_queue": 0,
                    "c_due": index,
                    "c_ivl": 0,
                    "c_factor": 0, 
                    "c_reps": 0, 
                    "c_lapses": 0,
                    "c_left": 0,
                    "c_odue": 0,
                    "c_odid": 0, 
                    "c_flags": 0,
                    "c_data": ""
                }
                con.execute(CARDS_SQL, card);

# Taken from Anki
# See https://github.com/dae/anki/tree/master/anki

reComment = re.compile("(?s)<!--.*?-->")
reStyle = re.compile("(?si)<style.*?>.*?</style>")
reScript = re.compile("(?si)<script.*?>.*?</script>")
reTag = re.compile("(?s)<.*?>")
reEnts = re.compile("&#?\w+;")
reMedia = re.compile("(?i)<img[^>]+src=[\"']?([^\"'>]+)[\"']?[^>]*>")

def stripHTML(s):
    s = reComment.sub("", s)
    s = reStyle.sub("", s)
    s = reScript.sub("", s)
    s = reTag.sub("", s)
    s = entsToTxt(s)
    return s

def checksum(data):
    if isinstance(data, str):
        data = data.encode("utf-8")
    return sha1(data).hexdigest()

def fieldChecksum(data):
    # 32 bit unsigned number from first 8 digits of sha1 hash
    return int(checksum(stripHTMLMedia(data).encode("utf-8"))[:8], 16)

def stripHTMLMedia(s):
    "Strip HTML but keep media filenames"
    s = reMedia.sub(" \\1 ", s)
    return stripHTML(s)

def entsToTxt(html):
    # entitydefs defines nbsp as \xa0 instead of a standard space, so we
    # replace it first
    html = html.replace("&nbsp;", " ")
    def fixup(m):
        text = m.group(0)
        if text[:2] == "&#":
            # character reference
            try:
                if text[:3] == "&#x":
                    return chr(int(text[3:-1], 16))
                else:
                    return chr(int(text[2:-1]))
            except ValueError:
                pass
        else:
            # named entity
            try:
                text = chr(name2codepoint[text[1:-1]])
            except KeyError:
                pass
        return text # leave as is
    return reEnts.sub(fixup, html)


# -- End code from Anki

if len(sys.argv) != 2:
    print("Usage: ./convertToApkg.py input.csv")
    print("There must exist a input.json containing the configuration")
    sys.exit()

tempdir = mkdtemp()
tablepath = tempdir + "/collection.anki2"

input = sys.argv[1]
last_slash = input.rfind("/") + 1
config = sys.argv[1][:-3] + "json"
gconfig = dir_path = os.path.dirname(os.path.realpath(__file__)) + "/generalconfig.json"
output = os.getcwd() + "/" + input[last_slash:-3] + "apkg"

print(input)
print(config)
print(output)


with open(config, 'r') as config_file: 
    configuration = json.loads(config_file.read())

with open(gconfig, 'r') as gconfig_file: 
    generalconfig = json.loads(gconfig_file.read())

create_tables(tablepath)
create_col(tablepath)
create_notes(tablepath, input)

with open(tempdir + "/media", 'w') as file:
    file.write("{}")

with zipfile.ZipFile(output, 'w', zipfile.ZIP_DEFLATED) as zf:
    zf.write(tablepath, "collection.anki2")
    media = {}
    if "image_dir" in configuration:
        directory = sys.argv[1][:last_slash] + "/" +  configuration["image_dir"] + "/"
        for _, _, f in os.walk(directory):
            for i, v in zip(range(len(f)), f):
                zf.write(directory + v, str(i))
                media[i] = v
    with open(tempdir + "/media", 'w') as file:
        file.write(json.dumps(media))
    zf.write(tempdir + "/media", "media")


shutil.rmtree(tempdir)

