+++
fragment = "contact"
#disabled = true
date = "2019-08-01"
weight = 1100
#background = "light"
form_name = "defaultContact"

title = "Kontakt"
subtitle  = "*Fragen, Verbesserungsvorschläge, Fehlermeldungen und alles andere*"

# PostURL can be used with backends such as mailout from caddy
post_url = "https://www.speicherleck.de/cgi-bin/mail.pl" #default: formspree.io
method = "POST"
email = "alex.mai@posteo.net"
button = "Nachricht senden" # defaults to theme default
#netlify = false

# Optional google captcha
#[recaptcha]
#  sitekey = ""

[message]
  success = "Nachricht abgeschickt! :-)" # defaults to theme default
  error = "Nachricht abgeschickt :-)" # defaults to theme default
# Nachricht konnte nicht verschickt werden. Wir sind auch unter alex.mai@posteo.net erreichbar.

# Only defined fields are shown in contact form
[fields.name]
  text = "Dein Name *"
  type = "text"
  name = "name"
  error = "Bitte Namen eingeben" # defaults to theme default

[fields.email]
  text = "Deine E-Mail-Adresse *"
  type = "text"
  name = "email"
  error = "Bitte E-Mail-Adresse eingeben" # defaults to theme default

#[fields.phone]
#  text = "Your Phone *"
  #error = "" # defaults to theme default

[fields.message]
  text = "Deine Nachricht *"
  type = "text"
  name = "message"
  error = "Nachricht ist leer" # defaults to theme default

# Optional hidden form fields
# Fields "page" and "site" will be autofilled
[[fields.hidden]]
  name = "page"

[[fields.hidden]]
  name = "someID"
  value = "example.com"
+++
