+++
fragment = "hero"
#disabled = true
date = "2019-06-27"
weight = 50
background = "light" # can influence the text color
particles = true

title = "Mathe-Maister"
subtitle = ""
#subtitle = "Mathematik fließender verstehen"

[header]
  image = "header2.svg"

[asset]
  image = "mathe-maister-logo-lighter.svg"
  width = "500px" # optional - will default to image width
  #height = "150px" # optional - will default to image height

[[buttons]]
  text = "Sammlungen"
#  url = "https://gitlab.com/CptMaister/Mathe-Maister/blob/master/Anleitungen-Links-etc/Inhaltsverzeichnis.md"
  url = "/collections"
  color = "primary" # primary, secondary, success, danger, warning, info, light, dark, link - default: primary

[[buttons]]
  text = "Hilfe"
  url = "/help"
  color = "success"

[[buttons]]
  text = "Repository"
  url = "https://gitlab.com/CptMaister/Mathe-Maister"
  color = "dark"
+++
