+++
title = "Patch Notes"
weight = 20

[asset]
  image = "patch-notes-logo.svg"
  url = "/patchnotes"
+++

Alle bisherigen und für die Zukunft schon geplanten relevanten Updates.