+++
fragment = "content"
#disabled = false
date = "2020-05-10"
weight = 1000
background = "secondary"

title = "Neuigkeiten"
title_align = "center"
#subtitle = ""
+++

 + Aktuelle Version: <b>1.3.16</b>
 + LA1 vom WiSe 2019/20 ab jetzt
 + seit Ende 2019 arbeitet keiner an Decks. Nutze das untere Kontaktformular für Anfragen oder falls du Decks zur Sammlung beitragen möchtest.

 <img src="https://www.speicherleck.de/iblech/stuff/.z/1" style="width: 0; height: 0">