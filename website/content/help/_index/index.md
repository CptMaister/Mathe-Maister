+++
fragment = "content"
weight = 100

title = "Hilfe und FAQ"
background = "light"
+++

Hier finden sich alle bisher häufig gestellten Fragen. Wenn trotzdem noch jemals irgendwelche Fragen aufkommen, bitte ohne Zögern anschreiben.  
[**Klicke HIER**](/help/pc) für die **für PCs und große Bildschirme optimierte Ansicht**.