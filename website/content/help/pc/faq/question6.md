+++
title = "> Wie funktioniert Anki, was kann ich da alles machen?"
weight = 60
+++

Wirklich wichtige Funktionen für dieses Projekt beschreibe ich im Folgenden, den
Rest kann man sich selbst aneignen durch Rumprobieren, auf
https://apps.ankiweb.net/docs/manual.html Nachschauen, Video-Tutorials
Anschauen, Blog Posts dazu Lesen, mich Fragen, ...  