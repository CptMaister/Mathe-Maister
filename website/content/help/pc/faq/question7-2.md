+++
title = "> Wie kann ich weniger/mehr als 20 Karten am Tag lernen? +++(NEU)+++"
weight = 72
+++

Wenn ein Deck frisch importiert wird (also nicht zum Aktualisieren eines schon vorhandenen Decks), dann sind die anstehenden neuen Karten pro Tag in der Regel 20 Stück (außer, wenn die Einstellungen der Optionengruppe `Default` mal verändert wurden, oder wenn es keine 20 neuen Karten im Deck gibt, oder wenn an dem Tag schon neue Karten gelernt wurden). Die Anzahl der neuen Karten pro Tag kann für jedes Deck einzeln eingestellt werden.  

Jedes Deck gehört zu einer Optionengruppe (ein neu importiertes Deck hat automatisch die Optionengruppe `Default`).  
Jede Optionengruppe hat eine Einstellung, wie viele neue Karten eines Decks, das zu dieser Optionengruppe gehört, pro Tag gelernt werden können.  
Um die neuen Karten eines einzelnen Decks zu ändern, solltest du also eine neue Optionengruppe erstellen (auf dem Handy: Im Hauptmenü rechts vom Decknamen auf das Feld mit den roten, grünen und blauen Zahlen tippen. Dann oben rechts auf das Symbol mit den 3 Punkten. Dann auf `Stapeloptionen -> Gruppenverwaltung -> Konfiguration hinzufügen` und diese beliebig benennen.  
Dann bei den `Stapeloptionen` ganz oben auf `Optionengruppe` tippen und die neue (oder eine andere gewünschte) Optionengruppe auswählen.  
Dann noch bei `Stapeloptionen -> Neue Karten -> Neue Karten/Tag` das gewünschte Tageslimit einstellen.  

***Protip***: Manchmal, vor allem wenn man zum Ende des Decks gelangt, scheint es so, als würde der Zähler für heutige neue Karten nach jeder gelernten neuen Karte um 2 sinken. Das passiert, weil Anki (zumindest mit Standardeinstellungen) sowohl bei neuen Karten als auch bei Wiederholungen nie mehrere Lernrichtungen einer Notiz pro Tag anzeigt. Wenn zu einer neuen Karte also auch noch die Rückrichtung ganz neu zu lernen wäre (zusammen ergeben alle Karten, die die gleichen Informationen enthalten, eine **Notiz**), dann wird nur eine dieser Richtungen angezeigt und die andere Richtung auf den nächsten Tag verschoben.  

***Protip #2***: In den allgemeinen Einstellungen von Anki kann man einstellen, ob neue Karten in die Wiederholungen rein gemischt werden sollen, oder ob sie alle zusammen vor oder nach den Wiederholungen kommen sollen.  
Auf dem Handy: im Hauptmenü oder beim Lernen von links die Sidebar rausziehen oder oben links auf das Sidebar-Menü-Symbol tippen. Dann auf `Einstellungen -> Lernen -> Position neuer Karten`.  