+++
title = "> Was mache ich mit Karten, die ich nicht lernen will? +++(NEU)+++"
weight = 75
+++

Die meisten Decks sind meistens befüllt mit fast allen Definitionen und Sätzen aus der Vorlesung. Zögere auf jeden Fall nicht, Karten aus dem Deck zu entfernen, die dich nicht interessieren! Sonst halten sie dich nur auf beim Lernen.  

Um eine Karte aus dem Deck zu entfernen, kannst du während dem Lernen (auf dem Handy) bei der entsprechenden Karte oben rechts auf das Symbol mit den 3 Punkten tippen und dann `Aussetzen` oder `Notiz löschen` wählen. **Eine gelöschte Notiz würde aber wieder auftauchen, wenn man sich eine neue Version des Decks runterlädt**. Deswegen sollten ungewollte Karten lieber ausgesetzt werden. Diese bleiben nämlich im Deck, werden aber beim Lernen komplett ignoriert. Eine ausgesetzte Karte kann auch wieder eingesetzt werden, wird aber nicht von selbst wieder nicht-ausgesetzt, wenn man das Deck aktualisiert.  

Bei `Aussetzen` kann man dann noch `Karte dauerhaft aussetzen` oder `Notiz dauerhaft aussetzen` wählen. Bei `Karte dauerhaft aussetzen` wird nur diese Lernrichtung ausgesetzt. Bei `Notiz dauerhaft aussetzen` werden alle Richtungen (bei den Karten von Mathe-Maister gibt es nur höchstens zwei Richtungen) ausgesetzt.  