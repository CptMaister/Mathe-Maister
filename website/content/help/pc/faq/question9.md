+++
title = "> Wie füge ich neu veröffentlichte oder neuere Versionen von Karten zu meinem bestehenden Deck hinzu?"
weight = 90
+++

Einfach die neuste Version runterladen und öffnen/importieren. Es sollten alle 
Karten im alten Deck automatisch aktualisiert werden, die geändert wurden, und 
neue Karten hinzugefügt werden. Der Lernfortschritt geht dabei nicht verloren.  
Bitte eine Nachricht an mich, falls das mal nicht klappt, wir sind da noch am 
Rumprobieren! 