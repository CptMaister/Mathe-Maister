+++
title = "> Wie kann ich ein eigenes Deck erstellen oder ein bestehendes Deck erweitern?"
weight = 100
+++

Hier möchte ich nur den einfachsten Weg erklären. Kompliziertere Wege würden so viel Vorwissen verlangen, dass man es auch selbst rausfinden kann. Irgendwann bald gibt es hoffentlich auch Videos dazu, aber vorerst der einfachste Weg:

Beitragen zu einem Deck kann man ganz einfach, indem man im Ordner `Stoffsammlung` die entsprechende `CSV`-Datei runterlädt, diese bearbeitet und mir dann zuschickt per Mail oder, besser noch, per Merge Request hier auf gitlab.  

Welche Eigenheiten beim Erstellen von Inhalten zu beachten sind, kann man in den [Hinweisen zur Sammlungserstellung](/help/contribute) einsehen.  

Selbst ein Deck erstellen geht ähnlich einfach. Pro Deck muss jeweils ein Ordner für Bilder, eine `CSV`-Datei, eine `JSON`-Datei (Konfigurationsdatei) existieren. Gerne kann man sich einfach nur um die CSV-Datei kümmern, sie mir zuschicken, und ich mache das mit der Konfigurationsdatei und lege den Bilderordner an, in dem fortan alle anderen Bilder auch gelagert werden können. Die Konfigurationsdatei muss nur am Anfang erstellt werden und enthält als einzig interessante, später eventuell änderwerte Information die in Anki sichtbare Beschreibung und den Decknamen.  
Wichtig ist noch, dass der Name der neuen CSV-Datei nicht mit dem Namen eines schon existierenden Decks übereinstimmt. Da der Name jederzeit geändert werden kann, ohne Auswirkungen für Nutzer der Decks zu haben, soll das zunächst mal kein Problem darstellen, bis wir uns ein cleveres System zur Benennung überlegt haben (sobald dann die ersten verschiedenen Decks zu den gleichen Vorlesungen entstehen).  