+++
title = "> Meine Karten werden nicht richtig angezeigt, ich sehe nur unleserliche Zeichen und Brabbeleien"
weight = 80
+++

Auf dem Handy einfach kurz Internet anmachen, Deck nochmal neu öffnen, dann sollte
es funktionieren bis zum kompletten Schließen der App. Danach braucht man wieder
kurz Internet, damit der Code kurz laden kann. Auf Windows PCs funktioniert 
es bisher nur manchmal, kann aber manchmal behoben werden durch Download dieses 
[Add-Ons](https://ankiweb.net/shared/info/1280253613). Auf Linuxmint funktioniert es. 