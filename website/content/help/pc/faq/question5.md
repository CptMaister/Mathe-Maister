+++
title = "Motivation"
weight = 50
+++

Als Hobbyprojekt einiger weniger Studenten der Uni Augsburg entstehen momentan Karteikarten zu Definitionen und Sätzen aus vielen Vorlesungen. Diese sollen jedem Studenten ermöglichen, Erinnerung zur Entscheidung zu machen und sich im Studienalltag seltener mit Nachschlagen von schon wieder vergessenem beschäftigen zu müssen.

Um schon verstandenes oder stur auswendiglernbares effizient ins Langzeitgedächtnis zu bekommen, hat sich das Spaced Repetition System als besonders nützlich herausgestellt, d.h. man beschäftigt sich zwar regelmäßig mit den einzelnen Stofffetzen (z.B. Definitionen oder Vokabeln), aber jeweils nach zunehmend länger werdenden Intervallen (1 Tag, 4 Tage, 2 Wochen, ...). Die Zeitintervalle zwischen einzelnen Erinnerungen soll dabei stärker zunehmen, je besser man es noch in Erinnerung hatte.

Karteikarten eignen sich dafür sehr gut, aber klassische Papierkarteikarten nur bedingt:

 - nach kurzer Zeit hat man zu viele, um sie immer bequem dabei zu haben
 - es ist schwer, sie mit Freunden zu teilen oder gemeinsam zu bearbeiten
 - die Sortierung und Einordnung für die Lernsessions der nächsten Tage wird schnell unübersichtlich

Die Open Source App Anki (für Android: AnkiDroid) löst diese Probleme und bringt viele andere Vorteile mit sich. Anki ist dabei nur unsere schlaue Karteikartenbox: Karten zu verschiedensten Themengebieten können einfach angefertigt und unter anderem online geteilt werden. Die Erstellung ausführlicher, aber vor allem knackiger und präziser Karten zum Mathestudium ist das Ziel des Projekts Mathe-Maister.  

Diese Decks sind aber explizit nicht dafür geeignet, Konzepte neu zu erlernen, sondern sollten nur dafür verwendet werden, schon gelernte Konzepte nicht zu vergessen, oder neue Definitionen schnell zu verinnerlichen. Dafür wird man, durch Einsatz von Anki mit diesen Decks, passend an genaue Formulierungen erinnert und muss dann, nach eigenem Ermessen, diese streng genug wiedergeben.  

*Extra*: [Interaktiver Webcomic zum Spaced Repetition System](https://ncase.me/remember/)  