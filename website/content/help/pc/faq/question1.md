+++
title = "> Wie komme ich in den Genuss der Mathe-Maister Decks?"
weight = 10
+++

- **Anki runterladen** (zum Beispiel AnkiDroid im Play Store). Damit hat man die
  Software, die mit den erstellten Karten etwas anfangen kann. Das Programm ist
  anfangs komplet leer, da Anki selbstverständlich nicht weiß, was man lernen
  will!
- Gewünschte **Decks runterladen und importieren** (siehe unten). Alle mathematischen Inhalte,
  meine Designentscheidungen zum Lernerlebnis, und LaTeX-Support (braucht
  bedingt Internet, siehe unten), sind schon enthalten.
- einfach **loslernen und täglich dranbleiben**
