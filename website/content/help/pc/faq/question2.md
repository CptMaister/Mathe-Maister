+++
title = "> Wo kann ich Anki runterladen?"
weight = 20
+++

* für den PC: https://apps.ankiweb.net/
* für Android: ***AnkiDroid*** auf
  **[F-Droid](https://f-droid.org/packages/com.ichi2.anki/)** oder im **[Play
  Store](https://play.google.com/store/apps/details?id=com.ichi2.anki)**
* für iOS: nicht sicher, aber sollte ***AnkiMobile*** heißen. Kostet aber auch was. ACHTUNG: Bisher wurden keine Tests auf iOS Anki durchgeführt!
