+++
fragment = "content"
weight = 100

title = "Hilfe und FAQ"
background = "light"
+++

Hier finden sich alle bisher häufig gestellten Fragen. Wenn trotzdem noch jemals irgendwelche Fragen aufkommen, bitte ohne Zögern anschreiben.