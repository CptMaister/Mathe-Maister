+++
fragment = "content"
#disabled = true
date = "2019-06-27"
weight = 100
#background = ""

title = "Patch Notes"
subtitle = "Alle bisherigen relevanten Updates"
+++

### Geplante Updates:
 - kleines Programm mit GUI, mit dem man lokal aus CSV-Dateien Decks, PDFs etc erstellen kann (mit Python Qt?)
 - Anleitungsvideos (auf YouTube?)
 - APKG Skript mit gewünschter Optionengruppe inklusive Leech Limit, Optionenname und tägliches Limit für Wiederholungen und neue Karten
 - APKG Skript mit richtiger Erstellungszeit (Created) statt Unix 0 (eher unwichtig)

### Ziel für Version 2.0.0
Alles muss richtig nutzerfreundlich sein. Jemand, der das ganz neu sieht, muss alles verstehen können mit den Texten (/Videos) auf der Seite. Vor allem auch muss die Erstellung und Bearbeitung von Sammlungen verständlich sein und die Erzeugung der APKGs lokal auf einem Computer ohne viel Installation funktionieren.

---

## Patch Notes


### V **1.4.0 ← 1.3.X** - 19-07-XX -- ***Website jetzt geil***
 - 
 - Anpassungen an Anki-Update: \\( \\) und \\[ \\] statt $ $ und $$ $$ für den LaTeX math mode
 - Website jetzt voll funktionsfähig, mit Listen und Grafiken und allem

### V **1.3.0 ← 1.2.10** - 19-06-29 -- ***funktionierende Website, CSV-Linter***
 - [Website](https://mathe-maister.de/) gibt's endlich :-)
 - [CSV-Linter](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/MMskripte/csv-linter.py) zur besseren und leichteren Fehlerbehebung, vor allem für zukünftige Beitragende. Ergebnis wird u.a. im Gitlab Job Log ausgegeben.
 - Patch Notes aufgehübscht mit Überschriften und Links

### V **1.2.0 ← 1.1.22** - 19-06-12 -- ***Bilder jetzt auch im PDF (+Mathematiker, +Anleitung, +Copyright-Vorkehrungen)***
 - Bilder in den PDFs, mit [neu erstelltem Docker](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/docker/latex-image/default.nix) für dieses Projekt :)
 - bei Karten mit Personennamen jetzt die [Person mit Lebenszeit und Nationalität](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/Stoffsammlung/Erw%C3%A4hnte-Mathematiker.txt) im Lesehinweis überall hinzugefügt
 - PDFs jetzt mit [neuem Header](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/MMskripte/convertCSVtoTEXAnkimathe.sh) mit Links zu diesem Projekt auf jeder Seite
 - [Anleitung zum Erstellen von Inhalten](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/README.md#-wie-kann-ich-ein-eigenes-deck-erstellen-oder-ein-bestehendes-deck-erweitern) überarbeitet
 - Korrektur LA1, Auslagerung von gemeinsamem Ana1 und LA Stoff in neues Deck: Ana LA Grundlagen
 - neue Vorlesungen: Topologie, Funktionentheorie, Analysis 1, (abgebrochen: Funktionalanalysis)
 - [Lizenzbedingungen](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/README.md#copyright-notice) überarbeitet im Readme

### V **1.1.0 ← 1.0.15** - 19-04-20 -- ***Bilder im Deck (+Lizenz, +schönere Karten)***
 - jetzt mit neuem Feld Illustration und das [APKG-Skript](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/MMskripte/convertToApkg.py) bindet die Bilder auch passend in die apkg ein
 - neues Deck: Seminar Geometrie von Flächen
 - neues Deck: Seminar zu Differentialformen (pausiert)
 - Komma Style fix, mit '$,' zu ' ,$'
 - jetzt mit conditional fields in den [card templates](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/MMskripte/generalconfig.json), damit nicht extra leere Zeilen dranstehen, wenn die optionalen Felder leer sind
 - jetzt mit [Lizenz](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/LICENSE)
 - Tags gefixt

### V **1.0.0 ← 0.8.10** - 19-03-09 -- ***Vollautomatisierung: APKG-Skript***
 - funktionierendes [APKG-Skript](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/MMskripte/convertToApkg.py) (paar Bugs, aber ok)
 - Ana3 und Geo Deck halb fertig, durch das APKG-Skript jetzt auch immer auf dem neusten Stand benutzbar
 - in CSVs Tags und IDs aktualisiert eingeführt
 - keine APKG oder sonstigen Dateien mehr im Repo, ab jetzt nur noch Inhalte sammeln und hier und da Sachen anpassen, evtl Bugfixes und neue Features.

### V **0.8.0 ← 0.7.12** - 19-02-12 -- ***Erste fertige Decks***
 - jetzt mit Patch Notes!
 - LA1 Sammlung fertig (bis auf Korrektur)
 - Numerik Sammlung vorerst fertig, schon korrigiert
 - [CSVtoTEX](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/MMskripte/convertCSVtoPDF.sh) jetzt um [AWK Skript](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/MMskripte/convertCSVtoTEXcore.awk) ergänzt, fehlt noch leere Zeilen weglassen
 - Style fix: Optionale Felder starten großgeschrieben und enden mit Punkt, wenn nicht im Mathe-Modus
 - Fehlen von "Lesehinweis" auf der Karten-Rückseite gefixt

### V **0.7.0** - 19-01-30 -- ***Usability-Tests***
 - Anki-Import Update Bedingungen fast komplett durchblickt
 - Anki-Decks ausgelagert (leicht verbuggt)
 - erstes veröffentlichtes Numerik 1 Deck
 - neues öffentliches LA1 Deck
 - PDF jetzt lesbarer formatiert, mehr Infos, Kommentarfunktionen
 - Umstrukturiert, Dokumente gesammelt, Readme ausgebaut

### V **0.6.0** - 19-01-22 -- ***README-Kunst***
 - Readme anspruchsvoller, mit [Special Thanks](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/README.md#special-thanks) und Emojis
 - erstes Merge Request
 - neue Latex Skills (u.a. \operatorname)
 - mehr LA1, Numerik und Ana3

### V **0.5.0** - 19-01-17 -- ***Automatisierung PDFs***
 - [Skripte](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/MMskripte) für CSV → TEX → PDF
 - [Gitlab CI](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/.gitlab-ci.yml) für Skripte
 - [E-Mail-Verteiler](http://supersecret.me/ankiprojekt/) für Updates

### V **0.4.0** - 18-12-19 -- ***Nicht mehr nur Tests***
 - erstes veröffentlichtes Anki-Deck (aktuelle LA1 Vorlesung auf aktuellem Stand)
 - readme gefüllt mit Erklärungen für Nutzer

### V **0.3.0** - 18-12-16 -- ***GitLab***
 - Wechsel github → gitlab

### V **0.2.0** - 18-12-06 -- ***Besseres Fundament***
 - Numerik Test Anki-Deck
 - Notiztypen kombiniert, angepasst
 - Ordner umstrukturiert
 - neue Latex Skills (u.a. \limits)
 - Dateien angelegt und erster Erstellungsplan

### V **0.1.0** - 18-11-11 -- ***Erste Deckstruktur***
 - erstes Test Anki-Deck (Geometrie)
 - Anki-Karten jetzt mit Latex durch [Mathjax Skript](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/Anleitungen-Links-etc/Anki-Note-Templates/Mathjax-Script.txt)
 - [Notiztypen](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/Anleitungen-Links-etc/Anki-Note-Templates/Notiztyp-Mathe-Maister) festgelegt und festgehalten

### V **0.0.0** - 18-10-20 -- ***Anfang***
 - angefangen
