+++
fragment = "content"
#disabled = true
date = "2020-05-10"
weight = 100
#background = ""

title = "Stoffsammlungen"
subtitle = "Hier sind alle Anki-Decks und PDFs! :-) Jegliche Arbeit an Mathe-Maister ist gerade pausiert, da sich vorher dafür verantwortliche aktuell leider zeitlich nicht damit beschäftigen können"
+++

## Anki-Decks
Inhaltliche Korrektheit ohne Gewähr, vollständige Sammlungen teilweise durch Promovierende o.ä. korrigiert:  

 + Pausiert:
  + [**Analysis I**](https://mathe-maister.de/Analysis-I.apkg)
  + [**Topologie**](https://mathe-maister.de/Topologie.apkg)
  + [**Mathe Basics** (Sammlung oft vergessener Tricks und Basics)](https://mathe-maister.de/Mathe-Basics.apkg)

 + Fertig oder abgebrochen bzw. nicht ganz fertig:
  + [**Ana LA Grundlagen** (fertig)](https://mathe-maister.de/Ana-LA-Grundlagen.apkg)
  + [**Lineare Algebra I** (fertig, WiSe 18/19 Uni Augsburg, bis ausschließlich Eigenwerte)](https://mathe-maister.de/Lineare-Algebra-I.apkg)
  + [**Numerik I** (fertig, WiSe 18/19 Uni Augsburg, bis ausschließlich Interpolation durch Splines)](https://mathe-maister.de/Numerik-I.apkg)
  + [**Analysis III**](https://mathe-maister.de/Analysis-III.apkg)
  + [**Geometrie I**](https://mathe-maister.de/Geometrie-I.apkg)
  + [**Funktionentheorie**](https://mathe-maister.de/Funktionentheorie.apkg)
  + [**Seminar Geometrie von Flächen**](https://mathe-maister.de/Seminar-Geometrie-von-Flächen.apkg)
  + [**Seminar zu Differentialformen**](https://mathe-maister.de/Seminar-Differentialformen.apkg)
  + [**Funktionalanalysis**](https://mathe-maister.de/Funktionalanalysis.apkg)


## PDFs  
Hier sind die Links zu für Menschen schön lesbaren PDFs aller Stoffsammlungen:

 + Pausiert:
  + [**Analysis I**](https://mathe-maister.de/Analysis-I.pdf)
  + [**Topologie**](https://mathe-maister.de/Topologie.pdf)
  + [**Mathe Basics** (Sammlung oft vergessener Tricks und Basics)](https://mathe-maister.de/Mathe-Basics.pdf)

 + Fertig oder abgebrochen bzw. nicht ganz fertig:
  + [**Ana LA Grundlagen** (fertig)](https://mathe-maister.de/Ana-LA-Grundlagen.pdf)
  + [**Lineare Algebra I** (fertig, WiSe 18/19 Uni Augsburg, bis ausschließlich Eigenwerte)](https://mathe-maister.de/Lineare-Algebra-I.pdf)
  + [**Numerik I** (fertig, WiSe 18/19 Uni Augsburg, bis ausschließlich Interpolation durch Splines)](https://mathe-maister.de/Numerik-I.pdf)
  + [**Analysis III**](https://mathe-maister.de/Analysis-III.pdf)
  + [**Geometrie I**](https://mathe-maister.de/Geometrie-I.pdf)
  + [**Funktionentheorie**](https://mathe-maister.de/Funktionentheorie.pdf)
  + [**Seminar Geometrie von Flächen**](https://mathe-maister.de/Seminar-Geometrie-von-Flächen.pdf)
  + [**Seminar zu Differentialformen**](https://mathe-maister.de/Seminar-Differentialformen.pdf)
  + [**Funktionalanalysis**](https://mathe-maister.de/Funktionalanalysis.pdf)
