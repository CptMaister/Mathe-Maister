+++
fragment = "content"
#disabled = true
date = "2020-05-10"
weight = 100
#background = ""

title = "Impressum"
#subtitle = ""
+++

Alexander Mai, Wolframstr 22d, 86161 Augsburg, alex.mai@posteo.net

Der Quelltext dieser Website ist öffentlich zugänglich auf 
<a href="https://gitlab.com/CptMaister/Mathe-Maister/tree/master/website">gitlab.com/CptMaister/Mathe-Maister</a>

---

**About Syna (this website's theme, generated with HUGO), provided with <3 by Okkur Labs**

Open Source Theme from Okkur for your next project.

Syna is based on the awesome work by digitalcraftsman with his Hugo Agency Theme.

The Hugo Agency Theme was based on the work of David Miller with his Startupbootstrap Agency Theme.