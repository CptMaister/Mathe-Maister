+++
fragment = "copyright"
#disabled = true
date = "2019-06-24"
weight = 1250
background = "dark"

copyright = "" # default: Copyright $Year .Site.params.name
attribution = true # enable attribution by setting it to true
+++
