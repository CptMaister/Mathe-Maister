+++
fragment = "nav"
#disabled = true
date = "2019-06-27"
weight = 0
background = "dark"

[repo_button]
  url = "https://gitlab.com/CptMaister/Mathe-Maister"
  text = "GitLab Repo" # default: "Star"
  icon = "fab fa-gitlab" # defaults: "fab fa-github"

# Branding options
[asset]
  image = "mathe-maister-logo.svg"
  text = "Mathe-Maister"
+++
