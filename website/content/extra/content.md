+++
fragment = "content"
#disabled = true
date = "2019-07-27"
weight = 100
#background = ""

title = "Extra Kram"
subtitle = "für wenn man noch mehr Bock auf Mathe oder Anki hat"
+++

### Schöne Links zu schönen Inhalten
 + [Pi-](https://github.com/iblech/mathezirkel-kurs/blob/master/mathecamp-2014/pi-lernen/pi-lernen.pdf), [Tau-](https://github.com/iblech/mathezirkel-kurs/blob/master/mathecamp-2014/pi-lernen/tau-lernen-en.pdf), [e-](https://github.com/iblech/mathezirkel-kurs/blob/master/mathecamp-2014/pi-lernen/e-lernen.pdf) und andere Kärtchen zum auswendig Lernen und Spaß haben [bei Ingo auf GitHub](https://www.ingo-blechschmidt.eu/outreach.html)  
 + interessante Vortragsfolien [ganz unten auf Ingos Website](https://www.ingo-blechschmidt.eu/outreach.html) (<- auf Videoportalen vom CCC oder YouTube nach Videos zu den Vorträgen suchen!)  
 + Anleitung zum [Fraktale Malen mit Python Turtle](https://gitlab.com/CptMaister/spass-mit-fraktalen/tree/master/Malen-in-Python-Turtle), ganz einfach und ohne Installation auf [trinket.io](https://trinket.io/) und extra Herausforderungen für [die Kochsche Schneeflocke](https://gitlab.com/CptMaister/spass-mit-fraktalen/blob/master/Malen-in-Python-Turtle/Kochsche-Schneeflocke.md) und mehr  
 + YouTuber [3blue1brown](https://www.youtube.com/channel/UCYO_jab_esuFRV4b17AJtAw), u.a. mit seiner coolen Playlist [Essence of Linear Algebra](https://www.youtube.com/watch?v=fNk_zzaMoSs&list=PLZHQObOWTQDPD3MizzM2xVFitgF8hE_ab)
 + YouTuber [Mathologer](https://www.youtube.com/channel/UC1_uAIS3r8Vu6JjXWvastJg)


### > Wie bleibe ich motiviert? / Wie kann ich die Lernerfahrung für mein leicht ablenkbares Gehirn angenehmer gestalten?
 + In den Optionen ausstellen, dass man während dem Lernen sieht, wie viele Karten noch übrig sind (lenkt sonst hart ab!)
 + [Puppy Reinforcement](https://ankiweb.net/shared/info/1722658993), ein Add-On, das einem ungefähr alle 10 Karten ein süßes Bild von einem Welpen zeigt. Enthält 50 Bilder, kann man aber nach Belieben erweitern und ersetzen. Häufigkeit auch umstellbar (Add-Ons sind generell nicht für mobile Anki-Versionen verfügbar, aber nicht vergessen: auf allen von mir bisher getesteten Linux Rechnern funktioniert das ganze Latex-Gerendere, also kann man auf einem Linux-Rechner genau so gut lernen, wie auf dem Handy, nur jetzt mit Hundewelpen!)
 + [Review Heatmap](https://ankiweb.net/shared/info/1771074083), ein Add-On, mit dem man zur gesamten eigenen Anki-Sammlung und zu einzelnen eigenen Decks die bisherigen und voraussichtlichen Wiederholungen übersichtlich betrachten kann. Sehr motivierend, da keine Lücke reinzubekommen!
 + [Progress Bar](https://ankiweb.net/shared/info/2091361802), *a progress bar that shows your progress in terms of passed cards per review session.*
 + [Life Drain](https://ankiweb.net/shared/info/715575551), *this add-on adds a life bar during your reviews. Your life reduces within time, and you must answer the questions in order to recover it.* Schön motivierend und auch so einstellbar, dass man kein Leben zurückgewinnt. Damit kann man sich ganz schön einen Timer einstellen, zum Beispiel fürs Lernen nach der [Pomodoro-Technik](https://de.wikipedia.org/wiki/Pomodoro-Technik).
 + [Night Mode für am Rechner](https://ankiweb.net/shared/info/1496166067).
 + ...mehr gibts hier nicht. Schick mir deine Lieblingstricks und -Add-Ons!


### > Kennst du noch andere geile Anki-Decks?
Hier sind ein paar cool aussehende Decks für zwischendurch (habe ich größtenteils selbst noch nicht getestet):
[Ultimate Geography](https://ankiweb.net/shared/info/2109889812), 
[Countries of the World](https://ankiweb.net/shared/info/2915332392), 
[100 Colors](https://ankiweb.net/shared/info/2033234340), 
[Great Works of Art](https://ankiweb.net/shared/info/685421036), 
[Piano Basics](https://ankiweb.net/shared/info/347130449), 
[Vogelstimmen](https://ankiweb.net/shared/info/2088996377), 
[NATO phonetic alphabet](https://ankiweb.net/shared/info/766972333), 
<b>[Sternbilder](https://gitlab.com/CptMaister/anki-constellations-and-more/blob/master/Anki-Decks/Sternbilder%20P1+2.apkg) (ein geiles Sternbilder Deck von Samuel und mir!)</b>, 
[Fonts characteristics and history](https://ankiweb.net/shared/info/1770056221), 
[Python code quiz](https://ankiweb.net/shared/info/51975584),
[Japanese Hiragana](https://ankiweb.net/shared/info/2183294427), 
[Katakana Reading Practice](https://ankiweb.net/shared/info/2015522924), 
[5000 most common French words](https://ankiweb.net/shared/info/468622242), 
[Periodic table mnemomincs](https://ankiweb.net/shared/info/490209917), 
[Knots and how to tie them](https://www.memrise.com/course/649284/knots-and-how-to-tie-them/) (ist zwar auf Memrise, aber [es gibt Anki Add-Ons](https://github.com/wilddom/memrise2anki-extension), die ein Memrise Deck komplett herunterladen und zu Anki hinzufügen können).