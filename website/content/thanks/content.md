+++
fragment = "content"
#disabled = true
date = "2019-06-28"
weight = 100
#background = ""

title = "Special Thanks"
#subtitle = ""
+++

<b>Besonderer Dank bei der Verwirklichung dieses Projekts geht an</b>  

+ [Daniel A](https://gitlab.com/esclear) &#128028; ([CSV Checker Skript](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/MMskripte/csv-checker.py))  
+ Felix W &#129417; (Early Testing)  
+ [Ingo Blechschmidt](https://gitlab.com/iblech) &#128034; (Korrektur Deluxe, Motivation, ehem. Deckspeicher, Website setup)  
+ Julia G &#128035; (Lineare Algebra I)  
+ [Kilian R](https://gitlab.com/kiandru) &#129412; (Docker, Readme-Layout-Spezialist, Blockchain Entrepreneur)  
+ Leonie N &#129416; (Motivation, Beratung)  
+ [Michael Struwe](https://gitlab.com/shak-mar) &#128056; ([PDF Skripte](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/MMskripte), [CI](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/.gitlab-ci.yml), git Setup, [Licensing](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/LICENSE))  
+ Moritz H &#128058; (Numerik I, [Mailing](http://supersecret.me/ankiprojekt/))  
+ [Rainer E](https://gitlab.com/fanaticae) &#128006; (SQL Hacker, Python-Connaisseur, [APKG Skript](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/MMskripte), [CI](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/.gitlab-ci.yml), Ana3)  
+ Roland M &#128009; (Korrektur Numerik I)  
+ [Xaver H](https://gitlab.com/xaverdh) &#128044; (1 Meister, [Docker](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/docker))  