+++
fragment = "content"
#disabled = true
date = "2019-06-27"
weight = 100
#background = ""

title = "Bekannte Probleme"
subtitle = "und was man dagegen machen kann"
+++

## Kompatibilitätsprobleme
<ul>
<li>Decks von Version <b>1.0.2 - 1.1.0</b> sind nicht automatisch kompatibel mit den aktuellen Decks. Das lässt sich aber ganz leicht beheben. Wie die Fehlermeldung "Aktualisierung ignoriert da der Notiztyp verändert wurde" nahelegt, wurde jedem Deck ein Feld für Bilder hinzugefügt. Deswegen einfach vor dem Import einer neueren Deckversion dem Notiztyp, der im Deck für die Karten verwendet wird, ein neues Feld mit dem Namen "Illustration" hinzufügen. Bei Problemen einfach mich ansprechen.</li>
<li>Sorry falls du dir ein Deck von <b>V1.0.0 - V1.0.2</b> runtergeladen hast, sind leider nicht mehr kompatibel mit zukünftigen Decks wegen einem Skriptfix! (kommt hoffentlich nicht mehr vor).</li>
<li><b>Vor Version 1.0.0</b> verfügbare Decks sind nicht mehr kompatibel mit der automatischen Aktualisierung durch den Anki-apkg-Import (Standard Deckimport durch Öffnen der Datei etc) der neuen Decks seit Version 1.0.0.  
Falls du ein Deck von vor Version 1.0.0 aktualisieren möchtest (also nicht über einen der Links hier drunter, sondern aus einem Ordner hier drüber runtergeladen), dann verwende den [unten Beschriebenen CSV-Import](#karten-aktualisieren-und-hinzuf%C3%BCgen-durch-csv-import).</li>
</ul>

---

## Karten Aktualisieren und Hinzufügen durch CSV-Import
<b>!!! Nur notwendig für Updates von Decks von vor Version 1 !!!</b>
<ul>
<li>CSV-Datei zur passenden Vorlesung in der [Stoffsammlung](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/Stoffsammlung) runterladen</li>
<li>in Anki auf das zu aktualisierende Deck klicken, dann oben links <b>File</b> → <b>Import...</b> und die Datei auf eurem Computer öffnen</li>
<li>ein neues Fenster ist in Anki offen, hier auf ein paar wichtige Sachen achten:
    <ul>
        <li>Ganz oben links bei `Type` den Notiztyp *Mathe-Maister Definitionen* (könnte leicht abweichen) wählen</li>
	    <li>Daneben bei `Deck` das Deck, in dem ihr die Karten aktualisieren oder hinzufügen wollt</li>
	    <li>Gleich darunter `Fields seperated by: Tab` auswählen</li>
	    <li>Direkt darunter `Update existing notes when first field matches` wählen </li>
	    <li>Direkt darunter Das Häkchen ☑ setzen bei `Allow HTML in fields`</li>
	    <li>Alles hierunter, also die Feldzuweisungen, sollten stimmen</li>
	    <li>ganz unten auf *Import* klicken</li>
    </ul>
<li>in einem Ergebnisfenster seht ihr, wie viele Karten hinzugefügt und verändert wurden.  </li>
</ul>
Achtung: das erste Feld jeder Notiz muss genau übereinstimmen mit dem ersten Eintrag der Zeile, sonst entsteht eine neue Notiz, statt einem Update. Dafür nach dem Import einfach schauen, welche neue Notizen entstanden sind, entsprechende alte Notizen schnell anpassen, neu entstandene Karten wieder löschen, nochmal CSV-Import durchführen. Das ganze so oft durchführen, bis keine neue Karten mehr entstehen (nach 1 bis 2 mal hat man normalerweise alles erwischt).
