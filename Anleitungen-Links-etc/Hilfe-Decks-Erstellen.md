# Hinweise zum Erstellen von Decks in einer CSV-Datei:
 - Eine Auflistung aller möglichen Fehler in einer CSV-Datei (bis auf Tex-Fehler) bietet das Skript csv-linter.py, nimmt als Argument eine CSV-Datei (Nutzung von `$` und `$$` für den Mathemodus ist dort veraltet, mittlerweile `\( \)` und `\[ \]`).
 - Spalten der CSV-Datei richtig befüllen! Dafür als Beispiel in andere CSVs schauen
 - als Vorlage für die CSV-Datei am besten die dafür gedachte Vorlagedateien [bei den Stoffsammlungen auf GitLab](https://gitlab.com/CptMaister/Mathe-Maister/tree/master/Stoffsammlung) hernehmen: Die CSV-Datei heißt Vorlage-Stoffsammlung.csv, der Ordner mit den Bildern heißt Vorlage-Stoffsammlung, die Konfigurationsdatei heißt Vorlage-Stoffsammlung.json - alle am besten umbenennen, aber sie müssen alle den gleichen Namen haben (bis auf die Dateiendung, die bleibt einfach so).
 - Zeilen, die mit `#` beginnen, werden im PDF und in den Anki-Decks ignoriert
 - Zeilen, die mit "Hallo Freunde der" beginnen, werden nur im PDF ignoriert. Die erste Zeile jedes Decks sollte diese Zeile enthalten, also diese Anleitungskarte.
 - Leere Zeilen zwischen befüllten Zeilen bitte vermeiden, da sie sonst fehlerhaft im PDF auftauchen. Generell sollte jede nicht-fertige Zeile einfach mit `#` anfangen, wenn man sie nicht im PDF haben will, denn dann wird sie zweifelsfrei ignoriert.
 - Die Spalte ID darf prinzipiell mit zufälligen Zahlen (bis zu 5-stellig getestet, außerdem sollten bis zu 2 Nachkommastellen möglich sein ohne Probleme) gefüllt werden, aber bisher ist die Konvention, dass die Tausenderstelle das Kapitel des Stoffes des Decks angibt und die ersten 3 Stellen einfach aufzählen im Stoff des Kapitels. Die zwölfte Notiz des vierten Kapitels hätte also die ID 4012. Es ist von extremer Wichtigkeit, dass eine Definition (bzw. ein Satz oder was auch immer), die einmal fertiggestellt und hochgeladen wurde mit einer ID, nie wieder ihre ID ändert, da das zu unerwarteten Lernfortschrittverlusten führt bei Nutzern des Decks in Anki. Wird eine Definition also in der CSV-Datei in an eine andere Zeilenzahl in der CSV-Datei verschoben, dann unbedingt auch die ID mitverschieben (am besten einfach die ganze Zeile)
 - Damit eine Zeile vom APKG-Skript überhaupt beachtet wird (also in der Anki-Datei umgesetzt wird), müssen das erste, zweite, vierte und das ID-Feld ausgefüllt sein.
 - Latex-Mathemodus mit `\(x = 5\)` für Inline und `\[x = 5\]` für Display.
 - da Mathjax verwendet wird, geht weder `\newcommand` noch die Einbindung von Paketen mit `\usepackage`. Zum Testen, was alles funktioniert, einfach auf arachnoid.com/latex mal eingeben und schauen, was rauskommt
 - Anki erlaubt HTML, insbesondere den Einsatz folgender, aber auch aller andere HTML Stylingbefehle, wobei das aktuelle PDF-Skript nur die folgenden HTML-Befehle fehlerfrei in Latex übersetzen kann: 
    - `<br>` für Zeilenumbruch. Insbesondere sollten Zeilenumbrüche in der CSV-Datei nur als `\\` (also im Latex-Stil) geschrieben werden, wenn man sich gerade im Mathemodus befindet, ansonsten (also in normalem Text) sollte mit `<br>` eine neue Zeile gestartet werden.
    - `<b> fett </b>` zum fett schreiben
    - `<i> kursiv </i>` zum kursiv schreiben
    - `&nbsp;` für ein geschütztes Leerzeichen
 - Da `<` und `>` spezielle Zeichen in HTML sind, sind das die aktuell einzigen bekannten Zeichen, die wir gesondert behandeln müssen, und zwar muss man immer `&gt;` statt `>` eingeben und `&lt;` statt `<`



## Anderes erwähnenswertes
 - den Namen einer Notiz nicht in der Beschreibung erwähnen
 - Wohin was gehört, kann man der Zeile entnehmen, die mit `#Name` anfängt
 - Feld "Name" beginnt immer groß, alle anderen Felder klein, wenn kein Nomen (also normale Rechtschreibung)
 - in der Spalte "einseitig" kann man `K1` (oder `C1` oder `1`) schreiben, damit nur die Richtung von Name zu Beschreibung als Karte erzeugt wird, aber nicht die andere Richtung. Für das Gegenteil einfach `K2` (oder `C2` oder `2`) schreiben
 - die Spalte Priorität hat aktuell noch keinen Nutzen
 - Funktionennamen werden im Mathe-Modus passend markiert. Während bekannte Funktionen wie sin durch `\sin` entsprechend angepasst werden (also einfach durch ein `\`), müssen nicht allgemein bekannte Funktionen lokal zu Mathe-Operatoren gemacht werden:  
 	- Ist kein Umlaut in der Funktionsbezeichnung, so reicht `\operatorname{fantasiefunktion}`.  
	- Enthält sie Umlaute, so wird `\mathrm{fünktiön}` verwendet, wobei jetzt der Abstand direkt danach eventuell zu klein ausfällt und manuell hinzugefügt werden muss
 - keine griechischen Buchstaben verwenden, nur Zeichen aus ASCII und Umlaute und so lala Kram
 - Sonstige erwähnenswerte zu beachtende Schreibweisen:
	- `^{\circ}` statt °
 - Stilentscheidungen (bitte auch beachten, damit alles einheitlich bleibt):
	- `\int \limits_a^b` statt `\int_a^b` (gleiches für `\prod`, `\sum` etc), Unterschied auf [arachnoid.com/latex](https://arachnoid.com/latex/) anschauen
	- `\sin` statt `sin` (gleiches für andere bekannte mathematische Operatoren)
	- `\operatorname{Rang}(x)` statt `Rang(x)` (gleiches für andere nicht in mathjax bekannte mathematische Operatoren)
	- `\ast` statt *
	- `\dots` oder `\ldots` statt ...


### Reihenfolge der Felder:
Name  
Beschreibung  
X Notizen  
Vorlesung  
Typ  
Themenbereich  
Formel  
Interpretation  
FunFacts  
Trivia  
Beispiele  
Lesehinweis  
Extras  
Literaturhinweis  
Übersetzung  
X Lernhinweis (F)  
X Lernhinweis (B)  
Illustration  
Tags  
ID  
einseitig  
Priorität  


## Interessante Links zum Bearbeiten
### 20 rules of formulating knowledge:
https://www.supermemo.com/en/articles/20rules

### Mathjax hier testen, Spacing leicht abweichend: 
https://arachnoid.com/latex/

### Interessante Links zu Mathe in Anki:
http://augmentingcognition.com/ltm.html  
https://www.reddit.com/r/Anki/comments/43mf83/guide_how_to_anki_maths_the_right_way/

### Tims Uni-Spicker:
http://timbaumann.info/uni-spicker/



## Zu den json-Konfigurationsdateien:
Alle random strings und integers auf [random.org](https://www.random.org/) gut generierbar. alles hier random generierte darf auf keinen Fall mit random generierten Werten aus anderen Konfigurationsdateien übereinstimmen, deswegen immer einen großen Bereich (vielleicht nicht über 64bits hinaus) wählen beim Generieren! Und auch hier drunter alle unterschiedlich.  

 - `model_id`: random int, zB 1398130163168  
 - `model_name`: Notiztypname  
 - `deck_id`: random int, zB 1550006797833  
 - `start_note_id`: random int  
 - `start_card_id`: random int  
 - `guid_random_string`: random string  
 

## Noch zu erledigen:
 - := durch (in Anki bzw Mathjax darstellbares) gescheites := ersetzen mit zentriertem Doppelpunkt
 - bessere Lösung finden für Operatornamen mit Umlauten statt mathrm