# Mathe-Maister Hilfe

## > Wie komme ich in den Genuss der Mathe-Maister Decks?
- **Anki runterladen** (zum Beispiel AnkiDroid im Play Store). Damit hat man die
  Software, die mit den erstellten Karten etwas anfangen kann. Das Programm ist
  anfangs komplet leer, da Anki selbstverständlich nicht weiß, was man lernen
  will!
- Gewünschte **Decks runterladen und importieren** (siehe unten). Alle mathematischen Inhalte,
  meine Designentscheidungen zum Lernerlebnis, und LaTeX-Support (braucht
  bedingt Internet, siehe unten), sind schon enthalten.
- einfach **loslernen und täglich dranbleiben**


### > Wo kann ich Anki runterladen?
* für den PC: https://apps.ankiweb.net/
* für Android: ***AnkiDroid*** auf
  **[F-Droid](https://f-droid.org/packages/com.ichi2.anki/)** oder im **[Play
  Store](https://play.google.com/store/apps/details?id=com.ichi2.anki)**
* für iOS: nicht sicher, aber sollte ***AnkiMobile*** heißen. Kostet aber auch was. ACHTUNG: Bisher wurden keine Tests auf iOS Anki durchgeführt!


### > Wie bleibe ich auf dem Laufenden zu großen Updates?
Dafür gibt es [*diesen E-Mail-Verteiler*](http://supersecret.me/ankiprojekt/), auf dem man sich jederzeit ein- und austragen kann.


## > Wo finde ich die Anki-Decks?
Auf [gitlab.com/CptMaister/Mathe-Maister](https://gitlab.com/CptMaister/Mathe-Maister)   

## Motivation
Als Hobbyprojekt einiger weniger Studenten der Uni Augsburg entstehen momentan Karteikarten zu Definitionen und Sätzen aus vielen Vorlesungen. Diese sollen jedem Studenten ermöglichen, Erinnerung zur Entscheidung zu machen und sich im Studienalltag seltener mit Nachschlagen von schon wieder vergessenem beschäftigen zu müssen.

Um schon verstandenes oder stur auswendiglernbares effizient ins Langzeitgedächtnis zu bekommen, hat sich das Spaced Repetition System als besonders nützlich herausgestellt, d.h. man beschäftigt sich zwar regelmäßig mit den einzelnen Stofffetzen (z.B. Definitionen oder Vokabeln), aber jeweils nach zunehmend länger werdenden Intervallen (1 Tag, 4 Tage, 2 Wochen, ...). Die Zeitintervalle zwischen einzelnen Erinnerungen soll dabei stärker zunehmen, je besser man es noch in Erinnerung hatte.

Karteikarten eignen sich dafür sehr gut, aber klassische Papierkarteikarten nur bedingt:
 - nach kurzer Zeit hat man zu viele, um sie immer bequem dabei zu haben
 - es ist schwer, sie mit Freunden zu teilen oder gemeinsam zu bearbeiten
 - die Sortierung und Einordnung für die Lernsessions der nächsten Tage wird schnell unübersichtlich

Die Open Source App Anki (für Android: AnkiDroid) löst diese Probleme und bringt viele andere Vorteile mit sich. Anki ist dabei nur unsere schlaue Karteikartenbox: Karten zu verschiedensten Themengebieten können einfach angefertigt und unter anderem online geteilt werden. Die Erstellung ausführlicher, aber vor allem knackiger und präziser Karten zum Mathestudium ist das Ziel des Projekts Mathe-Maister.  

Diese Decks sind aber explizit nicht dafür geeignet, Konzepte neu zu erlernen, sondern sollten nur dafür verwendet werden, schon gelernte Konzepte nicht zu vergessen, oder neue Definitionen schnell zu verinnerlichen. Dafür wird man, durch Einsatz von Anki mit diesen Decks, passend an genaue Formulierungen erinnert und muss dann, nach eigenem Ermessen, diese streng genug wiedergeben.  

*Extra*: [Interaktiver Webcomic zum Spaced Repetition System](https://ncase.me/remember/)  


#### > Wie funktioniert Anki, was kann ich da alles machen?
Wirklich wichtige Funktionen für dieses Projekt beschreibe ich im Folgenden, den
Rest kann man sich selbst aneignen durch Rumprobieren, auf
https://apps.ankiweb.net/docs/manual.html Nachschauen, Video-Tutorials
Anschauen, Blog Posts dazu Lesen, mich Fragen, ...  


#### > Wie kann ich ein runtergeladenes Deck in Anki reinstecken/importieren?
- Am **PC** genügt es normalerweise, nachdem Anki installiert wurde, einfach auf die Datei vom Deck  (sollte als Endung `.apkg` haben)  doppelt zu klicken. Alternativ per Drag&Drop in Anki reinziehen. Oder Anki  öffnen und in der Menüleiste auf File -> Import... und da dann das Deck auswählen und öffnen. Download eines Decks über Microsoft Edge führt oft zum Fehler, dass die Dateiendung geändert wird. Um dieses Problem zu lösen, einfach die Dateiendung zu `.apkg` ändern oder einfach [einen gescheiten Browser](https://www.mozilla.org/en-US/firefox/new/) nutzen.  
- Am **Handy** reicht es normalerweise auch, die Datei runterzuladen und dann zu öffnen. Bei mir zumindest wird es dann ohne Probleme von Anki erkannt und importiert. Es kommt zwar der Hinweis "Dies könnte lange dauern", aber das tut es nicht. Da wir die Decks unkonventionell erzeugen, erkennt Anki vermutlich die Datei nicht als korrektes Deck an und warnt sicherheitshalber vor Komplikationen. Falls es aufm Handy nicht klappt, aber aufm PC schon, wäre eine Möglichkeit, alle Decks immer aufm PC hinzuzufügen, und diese dann durch die Synchronisation (über ein kostenloses Anki-Konto) aufs Handy zu bekommen.


#### > Wie kann ich weniger/mehr als 20 Karten am Tag lernen?
Wenn ein Deck frisch importiert wird (also nicht zum Aktualisieren eines schon vorhandenen Decks), dann sind die anstehenden neuen Karten pro Tag in der Regel 20 Stück (außer, wenn die Einstellungen der Optionengruppe `Default` mal verändert wurden, oder wenn es keine 20 neuen Karten im Deck gibt, oder wenn an dem Tag schon neue Karten gelernt wurden). Die Anzahl der neuen Karten pro Tag kann für jedes Deck einzeln eingestellt werden.  

Jedes Deck gehört zu einer Optionengruppe (ein neu importiertes Deck hat automatisch die Optionengruppe `Default`).  
Jede Optionengruppe hat eine Einstellung, wie viele neue Karten eines Decks, das zu dieser Optionengruppe gehört, pro Tag gelernt werden können.  
Um die neuen Karten eines einzelnen Decks zu ändern, solltest du also eine neue Optionengruppe erstellen (auf dem Handy: Im Hauptmenü rechts vom Decknamen auf das Feld mit den roten, grünen und blauen Zahlen tippen. Dann oben rechts auf das Symbol mit den 3 Punkten. Dann auf `Stapeloptionen -> Gruppenverwaltung -> Konfiguration hinzufügen` und diese beliebig benennen.  
Dann bei den `Stapeloptionen` ganz oben auf `Optionengruppe` tippen und die neue (oder eine andere gewünschte) Optionengruppe auswählen.  
Dann noch bei `Stapeloptionen -> Neue Karten -> Neue Karten/Tag` das gewünschte Tageslimit einstellen.  

***Protip***: Manchmal, vor allem wenn man zum Ende des Decks gelangt, scheint es so, als würde der Zähler für heutige neue Karten nach jeder gelernten neuen Karte um 2 sinken. Das passiert, weil Anki (zumindest mit Standardeinstellungen) sowohl bei neuen Karten als auch bei Wiederholungen nie mehrere Lernrichtungen einer Notiz pro Tag anzeigt. Wenn zu einer neuen Karte also auch noch die Rückrichtung ganz neu zu lernen wäre (zusammen ergeben alle Karten, die die gleichen Informationen enthalten, eine **Notiz**), dann wird nur eine dieser Richtungen angezeigt und die andere Richtung auf den nächsten Tag verschoben.  

***Protip #2***: In den allgemeinen Einstellungen von Anki kann man einstellen, ob neue Karten in die Wiederholungen rein gemischt werden sollen, oder ob sie alle zusammen vor oder nach den Wiederholungen kommen sollen.  
Auf dem Handy: im Hauptmenü oder beim Lernen von links die Sidebar rausziehen oder oben links auf das Sidebar-Menü-Symbol tippen. Dann auf `Einstellungen -> Lernen -> Position neuer Karten`.  


#### > Was mache ich mit Karten, die ich nicht lernen will?
Die meisten Decks sind meistens befüllt mit fast allen Definitionen und Sätzen aus der Vorlesung. Zögere auf jeden Fall nicht, Karten aus dem Deck zu entfernen, die dich nicht interessieren! Sonst halten sie dich nur auf beim Lernen.  

Um eine Karte aus dem Deck zu entfernen, kannst du während dem Lernen (auf dem Handy) bei der entsprechenden Karte oben rechts auf das Symbol mit den 3 Punkten tippen und dann `Aussetzen` oder `Notiz löschen` wählen. **Eine gelöschte Notiz würde aber wieder auftauchen, wenn man sich eine neue Version des Decks runterlädt**. Deswegen sollten ungewollte Karten lieber ausgesetzt werden. Diese bleiben nämlich im Deck, werden aber beim Lernen komplett ignoriert. Eine ausgesetzte Karte kann auch wieder eingesetzt werden, wird aber nicht von selbst wieder nicht-ausgesetzt, wenn man das Deck aktualisiert.  

Bei `Aussetzen` kann man dann noch `Karte dauerhaft aussetzen` oder `Notiz dauerhaft aussetzen` wählen. Bei `Karte dauerhaft aussetzen` wird nur diese Lernrichtung ausgesetzt. Bei `Notiz dauerhaft aussetzen` werden alle Richtungen (bei den Karten von Mathe-Maister gibt es nur höchstens zwei Richtungen) ausgesetzt.  


#### > Meine Karten werden nicht richtig angezeigt, ich sehe nur unleserliche Zeichen und Brabbeleien
Auf dem Handy einfach kurz Internet anmachen, Deck nochmal neu öffnen, dann sollte
es funktionieren bis zum kompletten Schließen der App. Danach braucht man wieder
kurz Internet, damit der Code kurz laden kann. Auf Windows PCs funktioniert 
es bisher nur manchmal, kann aber manchmal behoben werden durch Download dieses 
[Add-Ons](https://ankiweb.net/shared/info/1280253613). Auf Linuxmint funktioniert es.


#### > Wie füge ich neu veröffentlichte oder neuere Versionen von Karten zu meinem bestehenden Deck hinzu?
Einfach die neuste Version runterladen und öffnen/importieren. Es sollten alle 
Karten im alten Deck automatisch aktualisiert werden, die geändert wurden, und 
neue Karten hinzugefügt werden. Der Lernfortschritt geht dabei nicht verloren.  
Bitte eine Nachricht an mich, falls das mal nicht klappt, wir sind da noch am 
Rumprobieren!


## > Wie kann ich ein eigenes Deck erstellen oder ein bestehendes Deck erweitern?
Hier möchte ich nur den einfachsten Weg erklären. Kompliziertere Wege würden so viel Vorwissen verlangen, dass man es auch selbst rausfinden kann. Irgendwann bald gibt es hoffentlich auch Videos dazu, aber vorerst der einfachste Weg:

Beitragen zu einem Deck kann man ganz einfach, indem man im Ordner `Stoffsammlung` die entsprechende `CSV`-Datei runterlädt, diese bearbeitet und mir dann zuschickt per Mail oder, besser noch, per Merge Request hier auf gitlab.  

Welche Eigenheiten beim Erstellen von Inhalten zu beachten sind, kann man in den [Hinweisen zur Stoffsammlung](/Anleitungen-Links-etc/Hilfe-Decks-Erstellen.md) einsehen.  

Selbst ein Deck erstellen geht ähnlich einfach. Pro Deck muss jeweils ein Ordner für Bilder, eine `CSV`-Datei, eine `JSON`-Datei (Konfigurationsdatei) existieren. Gerne kann man sich einfach nur um die CSV-Datei kümmern, sie mir zuschicken, und ich mache das mit der Konfigurationsdatei und lege den Bilderordner an, in dem fortan alle anderen Bilder auch gelagert werden können. Die Konfigurationsdatei muss nur am Anfang erstellt werden und enthält als einzig interessante, später eventuell änderwerte Information die in Anki sichtbare Beschreibung und den Decknamen.  
Wichtig ist noch, dass der Name der neuen CSV-Datei nicht mit dem Namen eines schon existierenden Decks übereinstimmt. Da der Name jederzeit geändert werden kann, ohne Auswirkungen für Nutzer der Decks zu haben, soll das zunächst mal kein Problem darstellen, bis wir uns ein cleveres System zur Benennung überlegt haben (sobald dann die ersten verschiedenen Decks zu den gleichen Vorlesungen entstehen).  